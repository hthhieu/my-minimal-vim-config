" Map the leaderkey to space
let mapleader="\<SPACE>"

" Some tweaks
set showmatch		" Show matching brackets.
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.
set ignorecase          " Make searching case insensitive
set smartcase           " ... unless the query has capital letters.

" Open a new file
noremap <leader>ff :e <C-R>=expand("%:p:h") . "/" <CR>
" Save current buffer
noremap <leader>fs :w<CR>
" Reload the config file
noremap <leader>feR :source ~/.vimrc<CR>
" Windows spliting
noremap <leader>w3 :vsplit<CR>
noremap <leader>w2 :split<CR>
noremap <leader>w0 :hide<CR>
" Windows navigation
noremap <leader>wo <C-w><C-w>
noremap <leader>wh :wincmd h<CR>
noremap <leader>wj :wincmd j<CR>
noremap <leader>wk :wincmd k<CR>
noremap <leader>wl :wincmd l<CR>
" Buffers switching
noremap <leader>bb :buffers<CR>:buffer<Space>

" Relative numbering
function! NumberToggle()
	if(&relativenumber == 1)
		set nornu
		set number
	else
		set rnu
	endif
endfunc

nnoremap <leader>r :call NumberToggle()<cr>

